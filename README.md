# Facebook Messenger for WordPress (Lite version)

**Scroll down to see download link!**

![Messenger for WordPress Intro](https://bitbucket.org/repo/xKXKX5/images/3077139109-intro-03.png)

![Play Video Demo on Youtube Now](https://bitbucket.org/repo/xKXKX5/images/1499701937-play-video-demo-03.png)

[Play Video Demo on Youtube Now](https://youtu.be/aRQNIANfxYY)

![NinjaTeam Support Center](https://bitbucket.org/repo/xKXKX5/images/973329433-support-01.png)

[NinjaTeam Support Center](https://ninja.ticksy.com)

![Why choose us](https://bitbucket.org/repo/xKXKX5/images/2132370703-why-02.png)

![Facebook Messenger for WordPress Features](https://bitbucket.org/repo/xKXKX5/images/487939452-features-04.png)

### HOW IT WORKS ###
Based on Facebook Messenger, this plugin runs like an instant messaging system. After setup, which is normally less than 2 mins, the blue badge of Facebook Messenger and the button Message Us on Facebook will appear on your pages.


Your customers find it convenient to ask about your products or services. 


This plugin is sure to help your business with time and cost saving, yet obtain a prompt reply to visitors. 


And what else, from those received messages, you will be able to build a rich customer base for online marketing and remarketing later on.

Cheers!

![Customers Review](https://bitbucket.org/repo/xKXKX5/images/1125116803-review-01.png)

### DOWNLOAD ###
**You can download at: [https://wordpress.org/plugins/wp-facebook-messenger](https://wordpress.org/plugins/wp-facebook-messenger)**

### CHANGELOG ###

**23/08/2016: (version 2.1)**

* Update: Upload icon/image popup
* Update: Default icon button
* Fixed: Small bugs

**27/06/2016: (version 2.0)**

* Added: More settings
* Added: Messenger icon position
* Added: Messenger icon and image type
* Added: Open Messenger app button on smartphone and tablet/ipad
* Update: Display header cover option
* Fixed: small CSS bugs
* Note: The effect in this version still not smooth, we will update it soon.

**15/06/2016: (version 1.5)**

* Update: Improved settings

**12/06/2016: (version 1.4)**

* Fixed: small CSS bugs

**10/06/2016: (version 1.3)**

* Added: Compatible with Facebook Live Chat plugin

**05/06/2016: (version 1.2)**

* Added: Compatible with RTL languages

**01/06/2016: (version 1.1)**

* Fixed: Change color for icon
* Fixed: Padding-right of the box

**25/05/2016: (version 1.0)**

* Version 1.0 Initial Release